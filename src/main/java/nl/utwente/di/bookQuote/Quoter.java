package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    double getBookPrice(String isbn){
        HashMap<String,Double> isbnResult = new HashMap<String, Double>();
        isbnResult.put("1", 10.0);
        isbnResult.put("2", 45.0);
        isbnResult.put("3", 20.0);
        isbnResult.put("4", 35.0);
        isbnResult.put("5", 50.0);
        if(isbnResult.containsKey(isbn)){
            return isbnResult.get(isbn);
        }
        else {
            return 0.0;
        }
    }

}
